package com.abbtech.bookstore_book_api.utils;

import com.abbtech.bookstore_book_api.dto.BookRequestDTO;
import com.abbtech.bookstore_book_api.dto.BookResponseDTO;
import com.abbtech.bookstore_book_api.dao.entities.Book;
import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class BookConverter {
    public static BookResponseDTO toDto(Book book) {
        return new BookResponseDTO(book.getId(), book.getTitle(), book.getAuthor(), book.getPrice());
    }

    public static List<BookResponseDTO> toDto(List<Book> books) {
        return books.stream().map(BookConverter::toDto).toList();
    }

    public static Book toEntity(BookRequestDTO bookRequestDTO) {
        Book book = new Book();
        book.setTitle(bookRequestDTO.title());
        book.setAuthor(bookRequestDTO.author());
        book.setPrice(bookRequestDTO.price());
        return book;
    }
}
