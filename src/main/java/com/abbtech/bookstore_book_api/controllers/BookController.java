package com.abbtech.bookstore_book_api.controllers;

import com.abbtech.bookstore_book_api.dto.BookRequestDTO;
import com.abbtech.bookstore_book_api.dto.BookResponseDTO;
import com.abbtech.bookstore_book_api.exception.ErrorDetailDTO;
import com.abbtech.bookstore_book_api.services.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Validated
@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
@Tag(name = "Book Controller", description = "APIs for managing book services including retrieving, adding, and viewing details of books.")
public class BookController {
    private final BookService bookService;

    @Operation(summary = "Get all books", description = "Retrieve a list of all books in the bookstore.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of books"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BookResponseDTO> getAll() {
        return bookService.getAll();
    }

    @Operation(summary = "Get book by ID", description = "Retrieve details of a book by its unique ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved book details"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookResponseDTO getById(@PathVariable UUID id) {
        return bookService.getById(id);
    }

    @Operation(summary = "Add a new book", description = "Add a new book to the bookstore with the provided details.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Book successfully added"),
            @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookResponseDTO add(@Valid @RequestBody BookRequestDTO book){
        return bookService.add(book);
    }
}
