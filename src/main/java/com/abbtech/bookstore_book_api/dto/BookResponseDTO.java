package com.abbtech.bookstore_book_api.dto;

import java.math.BigDecimal;
import java.util.UUID;

public record BookResponseDTO(
        UUID id,
        String title,
        String author,
        BigDecimal price)
{}
