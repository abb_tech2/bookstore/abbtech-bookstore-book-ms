package com.abbtech.bookstore_book_api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record BookRequestDTO(
        @NotBlank(message = "Title is mandatory") String title,
        @NotBlank(message = "Author is mandatory") String author,
        @NotNull(message = "Price is mandatory") BigDecimal price) {
}
