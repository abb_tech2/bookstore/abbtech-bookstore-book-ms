package com.abbtech.bookstore_book_api.services;

import com.abbtech.bookstore_book_api.dto.BookRequestDTO;
import com.abbtech.bookstore_book_api.dto.BookResponseDTO;

import java.util.List;
import java.util.UUID;

public interface BookService {
    List<BookResponseDTO> getAll();

    BookResponseDTO getById(UUID id);

    BookResponseDTO add(BookRequestDTO book);
}
