package com.abbtech.bookstore_book_api.services;

import com.abbtech.bookstore_book_api.exception.BadRequestException;
import com.abbtech.bookstore_book_api.exception.enums.BadRequestExceptionEnum;
import com.abbtech.bookstore_book_api.utils.BookConverter;
import com.abbtech.bookstore_book_api.dao.entities.Book;
import com.abbtech.bookstore_book_api.dao.repositories.BookRepository;
import com.abbtech.bookstore_book_api.dto.BookRequestDTO;
import com.abbtech.bookstore_book_api.dto.BookResponseDTO;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    @Transactional
    public List<BookResponseDTO> getAll() {
        log.info("Fetching all books");
        var books = bookRepository.findAll();
         return BookConverter.toDto(books);
    }

    @Transactional
    public BookResponseDTO getById(UUID id) {
        log.info("Fetching book with ID: {}", id);
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.BOOKS_NOT_FOUND));
        return BookConverter.toDto(book);
    }

    @Transactional
    public BookResponseDTO add(BookRequestDTO bookRequestDTO) {
        log.info("Adding new book: {}", bookRequestDTO.title());
        Book book = BookConverter.toEntity(bookRequestDTO);
        Book savedBook = bookRepository.save(book);
        return BookConverter.toDto(savedBook);
    }
}
