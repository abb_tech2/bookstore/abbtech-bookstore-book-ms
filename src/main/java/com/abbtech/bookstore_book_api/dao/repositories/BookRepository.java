package com.abbtech.bookstore_book_api.dao.repositories;

import com.abbtech.bookstore_book_api.dao.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findById(UUID id);
}
